﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace clase1EnC
{
    class Transaciones
    {
        private String tipoTransaccion;

        public String TipoTransaccion
        {
            get { return tipoTransaccion; }
            set { tipoTransaccion = value; }

        }
        private double valor;

        public double Valor
        {
            get { return valor; }
            set { valor = value; }
        }



        public double CalcularPorcentaje()
        {
            double resultado = 0.0;
            Cuenta cuenta = new Cuenta();
            if (cuenta.TipoCuenta == "Ahorros")
            {
                resultado = this.Valor * 0.3;
            }
            else
            {
                if (cuenta.TipoCuenta == "Plazo Fijo")
                {
                    resultado = this.Valor * 0.1;
                }
            }
                return resultado;
            }
        

            public void CalcularSaldo()
            {
                Cuenta cuenta = new Cuenta();
                cuenta.Saldo = this.Valor + CalcularPorcentaje();

            }

        }
    }


